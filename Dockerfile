FROM mcr.microsoft.com/azure-cli:2.61.0

WORKDIR /opt/azure-functions-core-tools

RUN wget https://github.com/Azure/azure-functions-core-tools/releases/download/4.0.5700/Azure.Functions.Cli.linux-x64.4.0.5700.zip -O release.zip && unzip release.zip && chmod +x /opt/azure-functions-core-tools/func /opt/azure-functions-core-tools/gozip && rm release.zip

ENV PATH="$PATH:/opt/azure-functions-core-tools"
